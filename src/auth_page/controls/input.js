import React, { Component } from 'react';

class Input extends Component{
    constructor(props){
        super(props);

        this.state = {value:''};
    }

    onChange = event => {
        this.setState({value:event.target.value});
    }

    render(){
        return <input type={this.props.type} id={this.props.id} className={this.props.className} placeholder={this.props.placeholder} value={this.state.value} onChange={this.onChange} onKeyPress={this.props.onKeyPress} />;
    }
}

export default Input;