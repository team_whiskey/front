import React, { Component } from 'react';
import Input from './controls/input';
import Button from './controls/button';
import axios from 'axios';
import hash from 'object-hash';
import './authPage.css';

class AuthPage extends Component{
    render(){
        return(
            <div id="auth_page">
                <div className="form-group">
                    <div className="group">      
                        <Input type="text" id="username_inpt" className="auth_input" placeholder="Username" />
                        <span className="bar"></span>
                    </div>
                    <div className="group">      
                        <Input type="password" id="password_inpt" className="auth_input" placeholder="Password" onKeyPress={(event) => event.key.toLowerCase() === 'enter' ? this.signIn() : null } />
                        <span className="bar"></span>
                    </div>
                    <div className="group">
                        <Button className="btn btn-danger btn-lg" value="Sign in" onClick={this.signIn} />
                    </div>
                    <div className="group">
                        <Button className="btn btn-outline-danger btn-sm" value="Register" onClick={this.register} />
                    </div>
                </div>
            </div>
        );
    }

    signIn = () => {
        console.log('sign in request');
        axios.post('*path to auth service*', {
            username: document.getElementById('username_inpt').value,
            password: document.getElementById('password_inpt').value     //hash(document.getElementById('password_inpt'))
        })
        .then(resp => {
            console.log(resp.data);
            sessionStorage.setItem('jwt', resp.data.jwt)
            this.props.updateJwt();
        })
        .catch(err => {
            console.log(err);
        });
    }

    register = () => {
        console.log('register request');
        axios.post('*path to auth service*', {
            username: '',
            password: hash('')
        })
        .then(resp => {
            console.log(this.resp);
        })
        .catch(err => {
            console.log(err);
        });
        this.props.updateJwt();
    }
}

export default AuthPage;