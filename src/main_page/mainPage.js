import React, { Component } from 'react';

class MainPage extends Component{
    render(){
        return (
            <div>
                <h3>main page</h3>
                <h4>welcome, {this.props.username}</h4>

            </div>
        );
    }
}

export default MainPage;