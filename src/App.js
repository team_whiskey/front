import React, { Component } from 'react';
import MainPage from './main_page/mainPage';
import AuthPage from './auth_page/authPage';
import jwt_decode from 'jwt-decode';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      jwt: null
    };

    //sample JWT: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InNtaXJub2ZmIiwianRpIjoiY2ZhMWU1NGEtOWU5Ni00OTJjLWFiMGItM2MzZGY1MmU4NmNlIiwiaWF0IjoxNTI2MzAxNTU2LCJleHAiOjE1MjYzMDUxNTZ9.5_Rjt91Av0ABBsITBrSBAUs5WeZxMtQWENEIDW6ShQU
  }

  componentWillMount(){
    this.updateJwt();
  }

  render() {
    let username = this.verifyJwt(this.state.jwt);
    console.log(username)
    return (
      <div className="App">
        {
          username
            ? <MainPage username={username}  />
            : <AuthPage updateJwt={this.updateJwt}  />
        }
      </div>
    );
  }

  updateJwt = () => {
    let jwt = sessionStorage.getItem('jwt');
    if(this.state.jwt !== jwt)
      this.setState({jwt});
  }

  verifyJwt = token => {
    try{
      let decodedToken = jwt_decode(token);
      return decodedToken.username;
    }
    catch(ex){
      return false;
    }
  }
}

export default App;
